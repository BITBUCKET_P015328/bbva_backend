'use strict'

const express=require('express');
const usuarioController=require('../controllers/usuario.controller');
const cuentaController=require('../controllers/cuenta.controller');
const movimientoController=require('../controllers/movimiento.controller');
const api = express.Router();

//USUARIO==CLIENTE
api.get('/usuarios',usuarioController.getUsuarios);
api.get('/usuario/:dni',usuarioController.getUsuario);
api.post('/usuario',usuarioController.saveUsuario);
api.put('/usuario/:dni',usuarioController.updateUsuario);
api.delete('/usuario/:dni',usuarioController.deleteUsuario);


//CUENTA
api.get('/cuenta/:dni',cuentaController.getCuentas);
api.post('/cuenta',cuentaController.saveCuenta);

//MOVIMIENTO
api.get('/movimiento/:cuenta',movimientoController.getMovimiento);
api.put('/movimiento/:cuenta',movimientoController.saveMovimiento);


module.exports=api;
