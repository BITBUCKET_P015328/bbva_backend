'use strict'

var requestJson = require('request-json');
const config=require('../config');

var urlMovimiento = config.mlab_host+config.mlab_db+config.mlab_collection_cuenta+'?' +config.mlab_key;
var movimientoMlab = requestJson.createClient(urlMovimiento);

//GET MOVIMIENTOS POR CUENTA
function getMovimiento(req,res){
    console.log("Get Movimientos");
    const queryName='&q={"cuenta":"'+req.params.cuenta+'"}';
    const filtro='&f={"movimiento":1,"id":1.0}';
    console.log(urlMovimiento + queryName +filtro);
    movimientoMlab.get(urlMovimiento + queryName + filtro , function(errM, resM, body) {
      res.send(body[0]);
    });
}

//PUT MOVIMIENTO
function saveMovimiento(req,res){
      console.log("Grabar movimiento");
      //valida input
      req.checkBody("importe", "Importe esta vacio").isDecimal();
      req.checkBody("tipo", "Tipo esta vacio").notEmpty();

      const queryName='&q={"cuenta":"'+req.params.cuenta+'"}';
      movimientoMlab.get(urlMovimiento + queryName , function(errG, resG, body) {
        console.log(urlMovimiento + queryName );
        let respon = body[0];
        console.log(respon);
        let newID = body[0].movimiento.length + 1;
        let importe = parseFloat(req.body.importe);
        let date = new Date();
        let day = date.getDate();
        let month = date.getMonth()+1;
        let year = date.getFullYear();
        let fechaActual =day+"/"+month+"/"+year;
        let newMov = {
          "id" : newID,
          "fecha" : fechaActual,
          "importe" : importe,
          "tipo" : req.body.tipo,
          "referencia": req.body.referencia
        };
        let cuenta = body[0];
        cuenta.movimiento.push(newMov);
        console.log(cuenta.saldo);
        cuenta.saldo = parseFloat(cuenta.saldo) + importe;
        const queryNamePush='q={"cuenta":"'+req.params.cuenta+'"}';
        const url = urlMovimiento + '&' + queryNamePush;

        console.log(url);
        movimientoMlab.put( url , cuenta, function(errP, resP, bodyP) {
        if(errP)
          console.log(errP);
        else
          res.send(bodyP);
        });
      });
};

module.exports={
  getMovimiento,
  saveMovimiento
};
