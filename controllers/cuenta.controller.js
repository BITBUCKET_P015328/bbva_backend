'use strict'

const config=require('../config');
const cuentaN = require('../models/cuenta.model');

var generate_id=0;

var requestJson = require('request-json');
var path = require('path');
//var urlUsuario="https://api.mlab.com/api/1/databases/mrevilla/collections/cuenta?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlCuenta = config.mlab_host+config.mlab_db+config.mlab_collection_cuenta+'?' +config.mlab_key;
var cuentaMlab = requestJson.createClient(urlCuenta);



//GET ALL CUENTAS
function getCuentas(req,res){
    console.log("Get todas las cuentas");
    const queryName='&q={"dni":"'+req.params.dni+'"}';

    cuentaMlab.get( urlCuenta + queryName , function(errG, resG, body) {
      var respuesta=body[0];
      if(undefined==respuesta)
         res.status(404).send({message:"Cliente no tiene cuentas"});
     else{
         console.log(respuesta);
        res.send(body);
     }

    });
}

//POST SAVE CUENTA
function saveCuenta(req,res){
    console.log("Creacion de Cuentas");
    console.log(req.body.dni);
    let nrooficina = Math.floor(Math.random() * (999 - 1) + 1);
    let nrocuenta = Math.floor(Math.random() * (9999999 - 1) + 1);
    let cuentaNew = new cuentaN({
      dni : req.body.dni,
      cuenta : '00110'+nrooficina.toString()+'020'+nrocuenta.toString(),
      moneda : req.body.moneda,
      producto : req.body.producto,
      subproducto : req.body.subproducto,
      desprod : req.body.desprod,
      dessubprod : req.body.dessubprod,
      saldo : 0.00,
      movimiento : []

    });
    cuentaMlab.post(urlCuenta, cuentaNew, function(errS, resS, body) {
      if(errS)
        console.log(errS);
      res.send(body);
    });
};

module.exports={
  getCuentas,
  saveCuenta
};
