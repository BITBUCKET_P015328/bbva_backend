'use strict'

//var requestJson = require('request-json');
const config=require('../config');
const usuarioN = require('../models/usuario.model');

//var mongoDB = require( 'mongoose' );
//const url=config.mlab_host+config.mlab_db+'collections/';

var generate_id=0;


var requestJson = require('request-json');
var path = require('path');
//var urlUsuario="https://api.mlab.com/api/1/databases/mrevilla/collections/usuario?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlUsuario = config.mlab_host+config.mlab_db+config.mlab_collection_usuario+'?' +config.mlab_key;
//var urlUsuarioDelete = config.mlab_host+config.mlab_db+config.mlab_collection_usuario;
var usuarioMlab = requestJson.createClient(urlUsuario);

//var mongoose = require('mongoose');
//var urlMongoDB = 'mongodb://admin123:admin123@ds023654.mlab.com:23654/mrevilla?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt';
//mongoose.connect(urlMongoDB, {
//  useCreateIndex: true,
//  useNewUrlParser: true
//});
//mongoose.connection.once('open', function(){
//        console.log('Conection has been made!');
//      }).on('error', function(error){
//        console.log('Error is: ', error);
//      });

//Get the default connection
//var db = mongoose.connection;


//GET ALL USERS
function getUsuarios(req,res){

       console.log("Get todos los Usuarios");
      usuarioMlab.get('', function(err, resM, body){
        if (err)
        {
          console.log(err);
        }
        else
        {
          res.send(body);
        }
      });

};

//GET UN usuario
function getUsuario(req,res){
    console.log("Get Usuario");
    var usuarioLogin = requestJson.createClient(urlUsuario);
    const queryName='q={"dni":"'+req.params.dni+'"}';
    const url = urlUsuario + '&' + queryName;
    console.log(url);
    usuarioMlab.get( url , function(err, resP, body) {
               var respuesta=body[0];
               if(undefined==respuesta)
                  res.status(404).send({message:"Usuario no Existe"});
              else{
                  console.log(respuesta);
                 res.send(respuesta);
              }
            });

};

//POST UN usuario
function saveUsuario(req,res){
  console.log("Save Usuario");
  //VALIDATE INPUT
    req.checkBody('dni', 'DNI esta vacio').notEmpty();
    req.checkBody('clave', 'Contraseña esta vacio').notEmpty();
    req.checkBody('estado', 'Ingresar un estado.').notEmpty();
    req.checkBody('nombre', 'Ingresar el nombre.').notEmpty();
    req.checkBody('apellido1', 'Ingresar primer apellido.').notEmpty();
    req.checkBody('apellido2', 'Ingresar segundo apellido.').notEmpty();
    req.checkBody('fecha_nacimiento', 'Ingresar fecha nacimiento.').notEmpty();
  //  req.checkBody('perfil', 'Ingresar perfil.').notEmpty();
    req.checkBody('correo', 'Ingresar correo electrónico.').notEmpty();

    var errors = req.validationErrors();
    if (errors) {res.status(400).send(errors);return;}

    let userNew = new usuarioN({
      _id: generate_id+1,
      dni: req.body.dni,
      clave: req.body.clave,
      estado: req.body.estado,
      nombre: req.body.nombre,
      apellido1: req.body.apellido1,
      apellido2: req.body.apellido2,
      fecha_nacimiento: req.body.fecha_nacimiento,
      perfil: req.body.perfil,
      correo: req.body.correo
    });
    console.log(userNew);
    usuarioMlab.post(urlUsuario, userNew, function(err, response, body) {
    if(err)
      console.log(err);
      res.send(body);
  });
    console.log("Fin Save Usuario");
};


//PUT USER
function updateUsuario2(req,res){
   console.log("Actualizacion de Usuario");
   console.log(req.params.dni);
   console.log(usuarioN);
   //console.log(JSON.stringify(req.body));
    usuarioN.findOneAndUpdate({"dni": req.params.dni }, { $set:
    //  {"clave": req.params.clave  }
    JSON.stringify(req.body)
  }, {new:true},
  //}, {upsert:true},
  //  },{'upsert': false,'new': false },
     function(err, doc) {
    //if (err) return next(err);
    //if (err) return console.error( JSON.stringify( err ) );
    //if (err) return  res.status(500).send(err);
    if (!doc) { console.log(doc);
      return  res.status(404).send("no se encontro")}
    console.log(doc);
    res.send('Usuario actualizado.');
    res.end();
});
};

function updateUsuario(req,res){
  console.log("Actualizacion de Usuario");
    req.check("clave", "La clave no debe ser vacio").notEmpty();

    var errors = req.validationErrors();
    if (errors) {res.status(400).send(errors);return;}

    const queryName='q={"dni":"'+req.params.dni+'"}';
    const url = urlUsuario + '&' + queryName;
    console.log(url);

    var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
    console.log(JSON.parse(cambio));

    usuarioMlab.put(url, JSON.parse(cambio), function(err, resM, body) {
    if (err)   console.log(err);
    res.send(body);
    });
};


//DELETE USER
function removeUsuario2(request,response){
//    console.log("Borrado de Usuario");
//     const queryName='q={"idCliente":'+request.params.id+'}&';
//     usuarioMlab.get(urlUsuario+'?'+queryName, function(err, res, body) {
//       var respuesta=body[0];
//       usuarioMlab.delete(urlUsuario+'/'+respuesta._id.$oid, function(errD, resD, bodyD) {
//         if(errD)
//          console.log(errD);
//         res.send(bodyD);
//       });
//     });

//   MongoClient.connect(server, function(error, db) {
//      if(error)
//        console.log("Error while connecting to database: ", error);
//      else
//        console.log("delete usuario successfully");

   //perform operations here
//   db.collection('usuario').deleteOne({"dni": request.params.dni });

//   db.close();
//   });

};

function deleteUsuario(req,response){
  console.log("Delete de Usuario");
   //var client = requestJson.createClient(url);
   const queryName='q={"dni":"'+req.params.dni+'"}';
   const url = urlUsuario + '&' + queryName;

 //const queryName='q={"dni":"'+req.query.dni+'"}';
  // const url = urlUsuario + '&' + queryName;
  console.log(url);
  // const queryName='q={"idCliente":'+request.params.id+'}';
   usuarioMlab.get(url, function(err, res, body) {
     console.log(body[0]);
     var respuesta=body[0];
     usuarioMlab.delete(config.mlab_host+config.mlab_db+config.mlab_collection_usuario+'/'+respuesta._id.$oid+'?'+config.mlab_key, function(errD, resD, bodyD) {
       if(errD)
        console.log(errD);
       response.send(bodyD);
     });
   });
};


module.exports={
  getUsuarios,
  getUsuario,
  saveUsuario,
  updateUsuario,
  deleteUsuario
};
