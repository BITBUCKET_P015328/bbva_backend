'use strict'

const  express = require('express');
const  bodyParser = require('body-parser');
const expressValidator = require('express-validator');

const config = require('./config');
const  api = require('./routes/usuario.routes');
const  app = express();

//var db = require('./models/db');

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
app.use(expressValidator());

app.use(function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin', '*'); 
    //res.setHeader('Access-Control-Allow-Origin', 'http://127.0.0.1:8081');  ,http://127.0.0.1:2000
    res.header('Access-Control-Allow-Headers','Origin,X-Requested-With,Content-Type,Accept');
    res.header('Access-Control-Allow-Methods','GET, POST, PUT, DELETE, OPTIONS, PATCH');
    next();
});

app.use(config.URLbase,api);
module.exports=app;
