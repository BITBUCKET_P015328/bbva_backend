const mongoose = require('mongoose');
//var db = require('../models/db');
const Schema = mongoose.Schema;


let CuentaSchema = new Schema({
   _id: { type: mongoose.Schema.Types.ObjectId },
    dni: { type: String, required: true,  unique: true },
    cuenta: { type: String, required : true },
    moneda: { type: String, required: true },
    producto: { type: String, required: true },
    subproducto: { type: String, required: true },
    desprod: { type: String, required: true },
    dessubprod: { type: String, required: true },
    saldo: { type: Number, min: 0 },
    movimiento:
    [
      { id: Number, fecha : Date, importe: Number, tipo: String, referencia:String }
    ]

});


// Export the model
module.exports = mongoose.model('cuenta', CuentaSchema);
