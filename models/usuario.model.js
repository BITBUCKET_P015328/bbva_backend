const mongoose = require('mongoose');
const Schema = mongoose.Schema;


let UsuarioSchema = new Schema({
   _id: { type: mongoose.Schema.Types.ObjectId },
    dni: { type: String, required: true,  unique: true },
    clave: { type: String, required: true  },
    estado: { type: String, required: true  },
    nombre: { type: String  },
    apellido1: { type: String  },
    apellido2: { type: String  },
    fecha_nacimiento: { type: Date },
    perfil: { type: String },
    correo: { type: String }
});


// Export the model
module.exports = mongoose.model('usuario', UsuarioSchema);
