
//set up mongoose connection
const mongoose = require('mongoose');
let urlMongoDB = 'mongodb://admin123:admin123@ds023654.mlab.com:23654/mrevilla?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt';
const mongoDB = process.env.MONGODB_URI || urlMongoDB;
mongoose.connect(mongoDB, {  useCreateIndex: true,  useNewUrlParser: true });
//mongoose.set("useFindAndModify", false);
mongoose.Promise = global.Promise;
let db = mongoose.connection;

// CONNECTION EVENTS
// When successfully connected
db.on('connected', function () {
  console.log('Mongoose default connection open to ' + urlMongoDB);
});

// If the connection throws an error
db.on('error',function () {
  console.log('MongoDB Connection Error. Please make sure that MongoDB is running.');
  process.exit(1);
});

// When the connection is disconnected
db.on('disconnected', function () {
  console.log('Mongoose default connection disconnected');
});

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function() {
  db.close(function () {
    console.log('Mongoose default connection disconnected through app termination');
    process.exit(0);
  });
});


// BRING IN YOUR SCHEMAS & MODELS
// For example
  require('./usuario.model');
// var usuario = mongoose.model('Book', UsuarioSchema);
//module.exports = mongoose;
